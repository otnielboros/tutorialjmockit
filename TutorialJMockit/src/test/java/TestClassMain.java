
import mockit.integration.junit4.JMockit;
import org.junit.*;

import static org.junit.Assert.*;
import mockit.*;
import org.junit.runner.RunWith;


@RunWith(JMockit.class)
public class TestClassMain {

    @Injectable
    SecondClass secondClass;

    @Tested
    MainClass mainClass;

    @Test
    public void testSetValue1(){
        assertTrue(mainClass.getValue() == 10);
        mainClass.setValue(11);
        assertTrue(mainClass.getValue() == 11);
    }

    @Test
    public void testSetValue2(){
        assertTrue(mainClass.getValue() == 10);
    }

}
