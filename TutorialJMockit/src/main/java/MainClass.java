public class MainClass {
    private int value;
    private SecondClass secondClass;

    public MainClass(){
        this.value = 10;
    }

    public MainClass(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
